//
//  ViewController.swift
//  Calc
//
//  Created by LinuxAcademy on 2016/10/31.
//  Copyright © 2016年 LinuxAcademy. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
	
	// ----- プロパティ -----
	// 電卓のディスプレイ部分のオブジェクト
	@IBOutlet weak var displayLabel: UILabel!
	// 電卓画面からの入力有無を判定するフラグ
	var usrInputting: Bool = false
	// マイナスの値かどうかを判定するフラグ
	var minusFlg: Bool = false
	// 入力されたボタンの値を保持する
	var selectedOperand: String = ""
	// 計算の対象とする数値を保持する
	var targetNumber: Double? = nil
	
	// 計算結果の数値を保持する（Computed Properties）
	var resultValue: Double {
		
		// getter
		get {
			// ディスプレイに表示している数値を取得してdouble型にキャスト
			return NSNumberFormatter().numberFromString(displayLabel.text!)!.doubleValue
		}
		
		// setter
		set {
			
			displayLabel.text = "\(newValue)"
			usrInputting = false
		}
	}

	
	// ----- 関数 -----
	override func viewDidLoad() {
		super.viewDidLoad()
	}

	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
	}
	

	// 関数①：数値入力処理
	@IBAction func inputNumber(sender: UIButton) {
		
		// 1. 変数displayNumを作成（初期値は”0”）
		var displayNum: String = "0"
		
		// 2. ボタンの入力状態（変数usrInputting）を判定
		if !usrInputting {
			
			// 2-1. false（入力開始）の場合
			// 2-1-1. 変数usrInputtingをtrue（入力途中）に変更
			usrInputting = true
			
			// 2-1-2. 変数senderからボタンの値を取得し、変数displayNumに代入
			displayNum = (sender.titleLabel?.text)!
			
		} else {
			
			// 2-2. true（入力途中）の場合
			// 2-2-1. 変数displayLabelから値を取得し、変数displayNumに代入
			displayNum = displayLabel.text!
			
			// 2-2-2. 保持したディスプレイの値にボタンの値を足す（文字列連結）
			displayNum += (sender.titleLabel?.text)!
		}
		
		// 3. 値をディスプレイに表示
		displayLabel.text = displayNum
	}
	
	
	// 関数②：演算ボタン処理
	@IBAction func operate(sender: UIButton) {
		
		// 1. ボタンの入力状態を判定
		if usrInputting {
			
			// 1-1. true（入力途中）の場合
			// 1-1-1. 関数③（enter関数）を実行
			enter()
		}
		
		// 2. タップされたボタンの値を保持
		selectedOperand = (sender.titleLabel?.text)!
	}
	
	
	//　関数③：演算処理
	@IBAction func enter() {
		
		usrInputting = false
		
		// 1. 計算結果を判定
		if targetNumber != nil && selectedOperand != "" {
			
			// 1-1. 計算結果を保持かつ演算ボタンの値が取得できる場合
			// 1-1-1. 変数resultValueの値を取得し、定数resultNumberに代入
			let resultNumber: Double = resultValue
			
			// 1-1-2. 演算ボタンの値によって処理を分岐
			switch selectedOperand {
				
			case "+":
				
				// 1-1-2-1. 演算ボタンが「+」の場合は計算結果に定数resultValueの値を加算
				resultValue = targetNumber! + resultValue
				
			case "-":
				
				// 1-1-2-2. 演算ボタンが「-」の場合は計算結果から定数resultValueの値を減算
				resultValue = targetNumber! - resultValue
				
			case "×":
				
				// 1-1-2-3. 演算ボタンが「×」の場合は計算結果と定数resultValueの値を乗算
				resultValue = targetNumber! * resultValue
				
			case "÷":
				
				// 1-1-2-4. 演算ボタンが「÷」の場合は計算結果と定数resultValueの値を除算
				// resultValueの値が「0」の場合の対応も実装（ゼロ除算対応）
				if resultNumber == 0 {
					
					return
				}
				
				resultValue = targetNumber! / resultNumber
				
			default:
				
				// 1-1-2-5. 上記以外の演算ボタンの場合は計算処理は行わない
				break
			}
		}
		
		selectedOperand = ""
		
		// 2. 計算結果を格納
		targetNumber = resultValue
	}
	
	
	// 関数④：＋/ー切り替え処理
	@IBAction func changeMinusVal() {
		
		// 1. 現在の値を判定
		if !minusFlg {
			
			// 1-1. 正の値（+）場合
			// 1-1-1. ディスプレイの値に「-」を追加
			displayLabel.text = "-" + displayLabel.text!
			minusFlg = true
			
		} else {
			
			// 1-2. 負の値（-）の場合
			// 1-2-1. ディスプレイの値から「-」を取る
			displayLabel.text =	displayLabel.text?.stringByReplacingOccurrencesOfString("-", withString: "")
			minusFlg = false
		}
	}
	

	// 関数⑤：入力消去処理（C）
	@IBAction func clear(sender: UIButton) {
		
		// 1. ディスプレイに表示する値に「0」をセット
		displayLabel.text = "0"
		
		// 2. ボタンの入力状態をfalse（入力開始）にセット
		usrInputting = false
		minusFlg = false
	}
	
	
	// 関数⑥：入力消去処理（AC）
	@IBAction func clearAll(sender: UIButton) {
		
		// 1. 計算結果に「0」をセット
		resultValue  = 0
		targetNumber = nil
		
		// 2. ボタンの入力状態をfalse（入力開始）にセット
		usrInputting = false
		minusFlg = false
		selectedOperand = ""
	}
}